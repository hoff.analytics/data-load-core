import os

DEBUG = False

LOGGING_FORMAT = '%(levelname)-7s%(asctime)s | %(name)-25s | %(message)s'
LOGGING_DIR = '/var/log/' if os.name != 'nt' else os.path.join(os.getcwd(), 'log')

# Создание каталога для логов в первый раз
if not os.path.exists(LOGGING_DIR):
    os.mkdir(LOGGING_DIR)

# configure access to google cloud via acc key
GOOGLE_SERVICE_ACCOUNT_KEY = ''
if GOOGLE_SERVICE_ACCOUNT_KEY != '':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = GOOGLE_SERVICE_ACCOUNT_KEY

# configure access to google cloud via app credentials (json service file)
GOOGLE_APPLICATION_CREDENTIALS_PATH = ''
if GOOGLE_APPLICATION_CREDENTIALS_PATH != '':
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = \
        GOOGLE_APPLICATION_CREDENTIALS_PATH

BQ_TB_COST = 5
