"""
log - Ведение логов
"""
import logging
import os
from datetime import date, datetime

from . import LOGGING_FORMAT, LOGGING_DIR, DEBUG

today = date.today()


class Log:
    """Объект для логирования"""
    def __init__(self, name: str = 'Unknown',
                 level: int = logging.DEBUG if DEBUG else logging.INFO,
                 console_output: bool = True,
                 file_output: bool = False):
        self.log_name_base = os.path.splitext(os.path.basename(name))[0]
        self.level = level
        self.now_day = datetime.now().day  # Сегодняшний день
        self.logger = logging.getLogger(self.log_name_base)
        self.logger.setLevel(level)
        self.formatter = logging.Formatter(
            fmt=LOGGING_FORMAT,
            datefmt='%Y.%m.%d %H:%M')
        if console_output:
            handler_stream = logging.StreamHandler()
            handler_stream.setFormatter(self.formatter)
            self.logger.addHandler(handler_stream)
        if file_output:
            file = os.path.join(LOGGING_DIR, f'{name}.log')
            logging_file_general = file
            handler_general = logging.FileHandler(
                logging_file_general, encoding='utf-8')
            handler_general.setFormatter(self.formatter)
            self.logger.addHandler(handler_general)
            self.logger.warning(f'Saving files to {LOGGING_DIR}!')
        self.warning(f'Logger initiated with level {self.level}!')

    @staticmethod
    def full_log_file_path(name):
        """Подготовка имени файла лога"""
        if name is not None:
            return os.path.join(LOGGING_DIR, f'{name}.log')
        else:
            return None

    def info(self, msg): self.logger.info(msg)

    def critical(self, msg): self.logger.critical(msg)

    def error(self, msg): self.logger.error(msg)

    def warning(self, msg): self.logger.warning(msg)

    def exception(self, msg): self.logger.exception(msg)

    def debug(self, msg): self.logger.debug(msg)
