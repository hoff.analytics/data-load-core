from abc import ABC
from datetime import datetime

import pandas.io.sql as psql
from google.cloud import bigquery
from google.cloud.bigquery import QueryJob
from google.api_core.exceptions import BadRequest
from google.cloud.exceptions import GoogleCloudError
from pandas import DataFrame

from . import BQ_TB_COST
from .log import Log


class GenericDataBase(ABC):
    """
    Abstract class for database.
    """

    class BadQueryDataName(Exception):
        pass

    class UnknownSrcFormat(Exception):
        pass

    def __init__(self, log: Log):
        # list of known queries for easier execution
        # example:
        # data = DBClass.get_data(query=DBClass.query_data_name(*params))
        # data = DBClass.get_data('data_name', *params)
        self.queries = {}
        # each database class must have connection to db
        self.cnxn = None
        self.log = log

    def get_data(self, data_name: str = None, query: str = None,
                 *args, **kwargs) -> DataFrame:
        """
        Method to get data from database.
        If generic data name passed as argument --
        db class will look in known queries, else --
        any query can be executed but without any
        guarantees.
        If impossible to find correct query
        for data name then UnknownDataNameError
        must be raised.
        If custom query execution fails --
        CustomQueryError raised.
        :param data_name:
        :param query:
        :return:
        """
        query = self.select_query(data_name, query, *args, **kwargs)
        return psql.read_sql(query, self.cnxn)

    def select_query(self, data_name, query=None, *args, **kwargs) -> str:
        if data_name is not None and data_name in self.queries:
            query = self.queries[data_name](*args, **kwargs)
        elif data_name is None and query is not None:
            # query passes as is
            pass
        else:
            raise self.BadQueryDataName(data_name)
        return query


class BigQuery(GenericDataBase):
    def __init__(self, log: Log, parent_script_name: str):
        GenericDataBase.__init__(self, log)
        self.client = bigquery.Client()
        self.session_cost = 0.0
        self.parent_script_name = parent_script_name

    def disconnect(self):
        self.log.info(f'Closing BQ instance for {self.parent_script_name}. '
                      f'Spent ${self.session_cost}.')
        query = f"""
            insert
                logs_activity.job_costs
            values (
                '{datetime.now().date()}',
                '{datetime.now().time()}',
                '{self.parent_script_name}',
                {self.session_cost})
        """
        if self.session_cost > 0:
            self.client.query(query=query).result()
        self.client.close()

    def _calculate_query_job_cost(self, query, job_config=None):
        # accessing protected attribute because it's the only way now to access
        # billing data (+ only processed field works)
        job_config.dry_run = True
        job_config.use_query_cache = False
        try:
            bytes_processed = float(
                self.client.query(
                    query=query, job_config=job_config
                ).total_bytes_processed)
        except:
            self.log.warning('Cannot calculate job cost!')
            bytes_processed = 0
        _cost = BQ_TB_COST * bytes_processed / 2 ** 40
        self.session_cost += _cost
        job_config.use_query_cache = True

    def get_data_async(self, data_name: str = None,
                       query: str = None, legacy=False,
                       force_execute=False,
                       *args, **kwargs) -> QueryJob.result:
        query = self.select_query(data_name, query, *args, **kwargs)
        config = bigquery.QueryJobConfig(use_legacy_sql=legacy)
        if not force_execute:
            self._calculate_query_job_cost(query, config)
        return self.client.query(
            query, job_config=config).result

    def put_local_data(self, file_path, dst_dataset,
                       dst_table, json_schema=None, autodetect=False,
                       time_partitioning=None, header=True,
                       write_disposition='WRITE_TRUNCATE',
                       project='hoff-ru'):
        table_ref = f'{project}.{dst_dataset}.{dst_table}'
        job_cfg = bigquery.LoadJobConfig()
        if '.csv' in file_path:
            job_cfg.source_format = bigquery.SourceFormat.CSV
            job_cfg.skip_leading_rows = 1 if header else 0
        elif '.json' in file_path:
            job_cfg.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
        else:
            raise self.UnknownSrcFormat(
                f'Cannot upload to GBQ: {file_path}. Unknown type')
        # schema usage needs refactoring (csv)
        job_cfg.schema = json_schema
        job_cfg.autodetect = autodetect
        job_cfg.write_disposition = write_disposition
        if time_partitioning is not None:
            job_cfg.time_partitioning = \
                bigquery.table.TimePartitioning(field=time_partitioning)
        with open(file_path, 'rb') as src_file:
            job = self.client.load_table_from_file(src_file,
                                                   table_ref,
                                                   job_config=job_cfg)
        try:
            job.result()
        except (BadRequest, GoogleCloudError):
            error_msg = f'Cannot put data {file_path} to ' \
                        f'database {dst_dataset}.{dst_table}'
            self.log.error(error_msg)
